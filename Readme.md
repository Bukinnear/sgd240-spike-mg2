---
title: Spike Report
---

MG 2 – Multiplayer in C++
=========================

Introduction
------------

We’ve made a basic Blueprint multiplayer game – so it’s time to try and
do it in C++.

Goals
-----

1.  The Spike Report should answer each of the Gap questions

This time, using the Top-Down Template (C++):

1.  Have most of the same capabilities as Spike MG-1, but now done in
    C++!

2.  Changes:

    a.  No shooting

    b.  No bomb-throwing

    c.  Instead, make it bomb “dropping” – like Bomberman!

        i.  Perhaps change the explosion of the bomb to match Bomberman
            style gameplay as well.

Additionally, add the following features:

1.  ~~A “blink” ability, with each player able to teleport a short
    distance forward ~~

2.  A Main Menu, with an entry field for an IP address and Port, to try
    and connect to for a specific Game.

3.  The server should wait for 2+ players to connect before starting the
    game (connected clients should be given spectator pawns by default,
    until the game starts).

4.  A countdown should appear on all clients’ screens before the game
    begins.

5.  Players should be able to Die and switch into a Spectator Pawn until
    the round is over, when a winner is declared – and then… nothing
    (we’ll add that in the next spike).

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- --------------------------------------
  Primary – Jared K   Secondary – Katie Hill, Matt Ponting
  ------------------- --------------------------------------

Technologies, Tools, and Resources used
---------------------------------------

-   AddMovementInput
    <https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/APawn/AddMovementInput/index.html>

-   CreateDefaultSubObject
    <https://docs.unrealengine.com/latest/INT/API/Runtime/CoreUObject/UObject/FObjectInitializer/CreateDefaultSubobject/1/index.html>

-   Spawning an Actor
    <https://answers.unrealengine.com/questions/328407/ive-been-trying-to-spawn-an-actor-in-c-for-the-las.html>

-   Spawning an Actor Unreal Docs
    <https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Actors/Spawning/>

-   Set Static Mesh
    <https://answers.unrealengine.com/questions/13555/question-apply-static-mesh-to-staticmesh-component.html>

-   Set Static Mesh Material
    <https://answers.unrealengine.com/questions/452750/how-to-change-a-static-meshs-material-in-c.html>

-   Run a function only on server
    [https://answers.unrealengine.com/questions/247200/reliable-and-run-on-server-in-c-code.html](%20https:/answers.unrealengine.com/questions/247200/reliable-and-run-on-server-in-c-code.html)

-   More detailed UFUNCTION properties
    <https://wiki.unrealengine.com/UFUNCTION>

-   Setting Timers
    [https://docs.unrealengine.com/latest//INT/Programming/UnrealArchitecture/Timers/index.html](https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Timers/index.html)

-   Get all Actors within range
    <https://answers.unrealengine.com/questions/193374/getting-actors-within-x-distance.html>

-   ApplyRadialDamage
    <https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Kismet/UGameplayStatics/ApplyRadialDamage/index.html>

-   Import UDamageType
    [https://answers.unrealengine.com/questions/111497/using-the-udamagetype-class.html](%20https:/answers.unrealengine.com/questions/111497/using-the-udamagetype-class.html)

-   Event Handling in C++ <https://wiki.unrealengine.com/Event_handling>

-   Replicating variables in C++
    [https://wiki.unrealengine.com/Networking/Replication\#C.2B.2B\_examples](https://wiki.unrealengine.com/Networking/Replication%23C.2B.2B_examples%20)

-   Replicated Variables Tips & Tricks
    <https://www.unrealengine.com/en-US/blog/network-tips-and-tricks>

-   Get blueprint class dynamically in C++
    <https://answers.unrealengine.com/questions/92651/get-blueprint-class-by-string-in-c.html>

-   It’s hard to use blueprint functions in C++
    <https://answers.unrealengine.com/questions/340641/how-to-instantiate-widget-in-c.html>

-   UE4 Widget in C++ tutorial (Battery Collector)
    <https://www.youtube.com/watch?v=jbM4VU8oPYI&t=321s>

-   UE4 C++ Programming UI with UMG
    <https://docs.unrealengine.com/latest/INT/Programming/Tutorials/UMG/index.html>

-   Referencing Assets in C++ (Importing/Finding/Loading from a String)
    <https://docs.unrealengine.com/latest/INT/Programming/Assets/ReferencingAssets/index.html>

-   Constructor Helpers (Referencing Assests during construction time)
    <https://docs.unrealengine.com/latest/INT/API/Runtime/CoreUObject/UObject/ConstructorHelpers/index.html>

-   UE4 Docs – CreateWidget
    <https://docs.unrealengine.com/latest/INT/API/Runtime/UMG/Blueprint/CreateWidget/index.html>

-   How to create a widget in C++?
    <https://answers.unrealengine.com/questions/130528/c-create-widget.html>

-   Create Widget using BP class (Useful)
    <https://forums.unrealengine.com/community/community-content-tools-and-tutorials/23466-tutorial-snippet-creating-a-umg-widget-in-c-and-delegate-example>

-   Referencing Particles in C++ (UParticleSystemComponent)
    <https://answers.unrealengine.com/questions/327192/how-do-i-reference-my-particle-system-in-code.html>

-   C++ Battery pickup example (UParticleSystemComponent)
    <https://github.com/aimert/Unreal-Projects/blob/master/BatteryCollector/Source/BatteryCollector/BatteryPickup.cpp>

-   Blueprint Multiplayer Server Browser (Shootout Tutorial)
    <https://docs.unrealengine.com/latest/INT/Resources/Showcases/BlueprintMultiplayer/index.html#blueprintwalkthrough>

-   Multiplayer and Sessions (theory)
    <https://docs.unrealengine.com/latest/INT/Programming/Online/Interfaces/Session/>

-   IOnlineSubsystem & AGameSession
    <https://answers.unrealengine.com/questions/24190/understanding-the-ionlinesubsystem-and-agamesessio.html>

-   Using Sessions in C++ (outdated)
    <https://wiki.unrealengine.com/How_To_Use_Sessions_In_C%2B%2B>

-   Bypassing Sessions in C++
    <https://answers.unrealengine.com/questions/29017/gamemode-multiplayer-c.html>

Tasks undertaken
----------------

### Replication

1.  See [here for general notes on replication](#general-information)

**Variables**

1.  Any variables we wish to replicate must be declared in the header
    using the UPROPERTY(Replicated) tag

2.  Additionally, we must implement the GetLifetimeReplicatedProps
    Function.

    -   See [Replicating&gt;Variables](#_Replication) for more details

**Functions**

1.  See [Replicating&gt;Functions](#_Functions) for details on
    implementing RPCs in C++

### Replace the movement system

1.  Movement as it is in the TopDown template will not work in a
    multiplayer environment, since it uses the navigation system, which
    will not work on client machines.

2.  Replace this by moving the actual movement function to the player
    pawn.

3.  Since we are using a Character pawn, we can use
    [AddMovementInput()](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/APawn/AddMovementInput/index.html) -
    See [here](#replication-1) for more details

### Dropping Bombs

1.  You will first need to create a bomb actor class for us to spawn.
    This simply inherits from the base AActor class

**Static Mesh**

1.  To create a static mesh (the visible component, and collider of our
    bomb), use
    [CreateDefaultSubObject](https://docs.unrealengine.com/latest/INT/API/Runtime/CoreUObject/UObject/FObjectInitializer/CreateDefaultSubobject/1/index.html).
    (The example code on the docs are outdated) - It should look
    something like:

BombMesh =
CreateDefaultSubobject&lt;UStaticMeshComponent&gt;(TEXT("BombMesh"));

1.  The above will be still be invisible unless you assign a shape &
    material to it. To do this, use FObjectFinder to import non-c++
    assets. See [here](#_Referencing_Non-C++_Assets) for more details

2.  Use BombMesh-&gt;SetStaticMesh and BombMesh-&gt;SetMaterial to set
    assign the mesh and material

3.  Set the scale (I used 0.75 for x, y, and z)

4.  Use BombMesh-&gt;SetCollisionResponseToX to edit the collider’s
    response to other actors

5.  Lastly, set the new mesh to be the RootComponent

**\
**

**Spawning **

1.  Use GetWorld()-&gt;SpawnActor&lt;TemplateClass&gt;(Location,
    Rotation) to spawn the actor from the Pawn. Make sure this is called
    on the server using an RPC. See [here](#functions) for how to do
    that.

**Particle System**

1.  Use FObjectFinder to import the particle system you wish to spawn.
    See [here](#_Referencing_Non-C++_Assets) for how to do that

2.  Use UGameplayStatics::SpawnEmitterAtLocation to spawn the particle

3.  **Note** I have found a race condition in that, if you spawn the
    particle, then destroy the actor, the particle will not spawn on the
    client machines.

4.  To counter this, use a timer to delay the destruction of this actor.
    More on that below.

**Timers**

1.  See [here](#setting-a-timer) for how to set a timer.

2.  Use a timer for the delayed explosion of the bomb

3.  To avoid issue with spawning the particles, delay the destruction of
    the bomb after the explosion, and hide it in the meantime.

**Dealing Damage**

1.  Ensure that the function that deals damage is an RPC to ensure
    consistency and integrity across all machines.

2.  Use UGameplayStatics::ApplyRadialDamage to deal damage

    a.  For the IgnoreActors argument required, just pass a blank
        TArray&lt;AActor\*&gt;

3.  On the pawn, you will need to subscribe to the OnTakeAnyDamage event
    to trigger damage functions. See [here](#_Subscribing_to_Events) for
    how to do that

### Scoring System

1.  See [here](#using-widgets-in-c) for creating/using widgets in c++

2.  Use PostLogin in the game mode to give the players a number

3.  Create a custom PlayerState that replicates the player’s score and
    number

4.  Create a widget that displays these variables.

5.  All UI elements should be done in the PlayerController, since they
    are only local relevant

6.  I added all players by getting all PlayerStates, creating a widget
    for them, and adding them to a TMap

7.  To refresh a specific player’s score, the PlayerState is used to
    search the TMap and update the specific UI

8.  You can use an OnRep function in the PlayerState to trigger a UI
    refresh, instead of updating every tick.

    a.  Note that the server will have to update differently, as OnRep
        functions do not affect it. This project did not cover this, as
        the scoring system is never properly used.

### Hosting/Joining games

1.  Create a main menu widget

2.  Use the level blueprint to spawn the menu

3.  See [here](#hostingjoining-games-1) for how to host/join games

4.  Use the target map’s blueprint to display a loading screen until 2
    players have joined. Then display a countdown and begin the round.

    a.  I used a timer to recursively call a function until the
        countdown hits 0

    b.  You can use GameMode::PostLogin to detect when a player joins –
        but be careful of race conditions between the player joining,
        and issuing commands to its’ objects (i.e begin round)

    c.  I used a multicast function on the GameState to issue the
        BeginRound command, since when I tried calling the begin round
        function on the playercontrollers on the server, it did weird
        things on the clients.

### Spectators

1.  Use bStartPlayersAsSpectators in the game mode to spawn the players
    as spectators

    a.  Since the player controllers are spawned as spectators, you can
        use the SpawnDefaultPawnFor in the game mode to create a new
        pawn, and use the player controller to Possess() it. See
        [here](#getting-a-list-of-playercontrollers-on-the-server) for
        how to get a list of all PlayerControllers on the server

What we found out
-----------------

### Replication

#### General Information

As a reminder, the following is a brush up on the networked layout of
UE4:

-   PlayerControllers exist only the remote client that owns it *and on
    the server*.

-   GameMode exists only on the server

-   PlayerStates have a **one to one ratio** with PlayerControllers, and
    are replicated to all machines.

-   There is **only one** GameState that is replicated to all machines

-   Variables that are not replicated are not guaranteed to reflect the
    same value across clients

-   Server RPCs will be sent to the server to be executed

-   Multicast RPCs execute on all machines, **but** *will only be
    executed if called on the server*

#### Variables

To replicate a variable to other clients, you must assign the
UPROPERTY(Replicated) tag in the header.

After this you must implement the GetLifetimeReplicatedProps function in
the CPP file. You do not need to declare this in the header, only in the
CPP file. The functions signature is as follows:

void ThisClass::GetLifetimeReplicatedProps(TArray&lt; FLifetimeProperty
&gt; & OutLifetimeProps) const

{

Super::GetLifetimeReplicatedProps(OutLifetimeProps);

DOREPLIFETIME(ThisClass, VariableToReplicate);

}

DOREPLIFETIME must be repeated for every variable that is
replicated.[]{#_Functions .anchor}

#### Functions

1.  To call an RPC in C++, you must specify the Server, or NetMulticast
    UFUNCTION tag in the header.

    -   If you are using and RPC, when you implement the function in the
        CPP file, you must add \_Implementation to the end of the
        function.

2.  You must then specify a Reliable, or Unreliable UFUNCTION tag.

3.  If you have the Reliable tag, you must also specify the
    WithValidation tag

    -   If you are using WithValidation, you must implement an
        additional function in the CPP file, with the same signature as
        the header, with \_Validate added on the end of the function
        name (instead of \_Implementation) and it must return a bool,
        typically true.

4.  Note that both the \_Implementation and \_Validate **must** have the
    *same argument signature as in the header declaration*

As an example, a server RPC would look like this:

**Header:**

UFUNCTION(Server, Reliable, WithValidation)

void FunctionName(ExampleArgumentType\* ExampleArgumentName);

**CPP:**

void ThisClass::FunctionName\_Implementation(ExampleArgumentType\*
ExampleArgumentName)

{

// Do things

}

bool ThisClass:: FunctionName \_Validate(ExampleArgumentType\*
ExampleArgumentName)

{

return true;

}

#### OnRep

You can additionally call a function when a variable is updated by the
server using the ReplicatedUsing UPROPERTY() tag, which will look
something like this

UPROPERTY(ReplicatedUsing=OnRep\_FunctionName)

VariableType VariableName;

UFUNCTION()

void OnRep\_FunctionName();

**Note:** Since this only takes place when the variable is updated by
the server, this will never be called on the server itself.

### Replicated Pawn Movement

To move to character class, use
[APawn::AddMovementInput](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/APawn/AddMovementInput/index.html).
A base pawn class will not automatically move on the data input, **but**
a *character* class will. The character will move in a given direction,
*not* along the entirety of the vector, so this must be done in the Tick
function.

By calling a function on the pawn to move it, it will be replicated to
the server, and to the other clients. Do not use an RPC to do this, or
it will result in very delayed responses to movement
commands.[]{#_Replication .anchor}

[]{#_Referencing_Non-C++_Assets .anchor}

### Referencing Non-C++ Assets

**FObjectFinder/FClassFinder**

To import non-c++ files into your project, the simplest way I have found
is to use ConstructorHelpers::FObjectFinder (or FClassFinder for
classes). This creates a ContructorHelper object that contains the
object or class of the asset you are looking for, amongst other things.
The typical usage of this will look like this:

static ConstructorHelpers::FObjectFinder&lt;ClassOfImportedObject&gt;
ConstructorHelperObject(TEXT("'/Game/Path/To/Your/Asset’"));

if (ConstructorHelperObject.Object) { YourVariable =
ConstructorHelperObject.Object; }

The class of the imported object can be found by hovering over the asset
in the content browser.

You can easily get the path to your asset by right clicking on the asset
in the content browser and selecting “Copy Reference”

**Note** if you are importing a blueprint object, you will need to edit
this reference, as the string it gives you will look like:

Blueprint'/Game/TopDownCPP/Blueprints/TopDownCharacter.TopDownCharacter'

and the ConstructorHelper will give errors in this format, so edit it so
that it looks like :

/Game/TopDownCPP/Blueprints/TopDownCharacter

### Setting a Timer

To set a timer, first, create an FTimerHandle VariableName object, then
call GetWorldTimerManager().SetTimer(VariableName, ObjectToCallOn,
&ClassName::FunctionToCall, TimeInSecondsFloat);

**Note** due to the nature of garbage collection, it is a good idea to
declare the FTimerHandle in the header where it is guaranteed to never
go out of scope (for the lifetime of the object,
anyway).[]{#_Subscribing_to_Events .anchor}

### Subscribing to Events

To subscribe to an event in C++, you must bind a function to the event
using

OnTakeAnyDamage.AddDynamic(ObjectToCallOn, &Class::Function);

For the function, do not put () the end of it

Visual Studio will not recognize AddDynamic and will suggest
\_\_AddDynamic\_Internal\_\_ just ignore that, it will compile anyway.

The function you specify must have the *same arguments signature* as the
event you are subscribing to. You will get an error about not being able
to assign something to the R-value if you get it wrong.

**Note**: You can find the signature to OnTakeAnyDamage at the top of
the AActor class.

### Using Widgets in C++

To use widgets in C++, you need to first edit your ProjectName.Build.cs
file. This can be found with your other c++ source files. Open that, and
where you see PublicDependencyModuleNames.AddRange - add

"UMG", "Slate", "SlateCore"

to the list.

Then create your widget. This is the same process as if you were working
in blueprint, however you will also create a c++ parent class that
inherits from the UUserWidget class, and re-parent the widget blueprint
to that.

Any functions or variables that you will need to access from c++ needs
to be declared in the parent class’s header, but not necessarily in the
cpp file. Make use of [UFUNCTION
tags](https://wiki.unrealengine.com/UFUNCTION) to expose these to
blueprint if you need to access them there.

To create your new widget in another c++ class, import your widget class
as you would a *blueprint class*. (See
[here](#_Referencing_Non-C++_Assets) for how to import non c++ files).
You may need to include Blueprint/UserWidget.h

static ConstructorHelpers::FClassFinder&lt;UUserWidget&gt;
WidgetObj(TEXT("/Game/Project/Blueprints/Widget"));

if (WidgetObj.Succeeded())

{

WidgetClass = WidgetObj.Class;

}

This class will serve as a template to create other widgets of the same
type.

Inside the PlayerController, use

CreateWidget&lt;WidgetClass&gt;(OwningPlayerController, WidgetClass)

To create your widget. Don’t forget to add it to the viewport when you
want to display it.

### Hosting/Joining games

To host a game, use

UGameplayStatics::OpenLevel(ContextObject, "MapName", true, "listen");

To join a hosted map, use

UGameplayStatics::OpenLevel(this, IPAddressString, true);

### Getting a List of PlayerControllers on the server

Use GetWorld-&gt;GetPlayerControllerIterator() to get an iterator
containing all of the PlayerControllers.

To use the Iterator, you can make use of the operator() functions on it,
for example

Iterator.operator bool()

will return true if the iterator has not reached it’s end. It may not be
necessary to directly call this function like this, and instead simply
query the bool value of the iterator, but I have not tested that.

You can use Iterator-&gt;Get() to retrieve the value that the iterator
is currently pointing to and ++Iterator to advance it to the next value.

\[Optional\] Open Issues/risks
------------------------------

-   I did not have time to complete the blink ability that was requested
    in the spike gap

    -   See
        [here](https://docs.unrealengine.com/latest/INT/Gameplay/Networking/CharacterMovementComponent/index.html#advancedtopic:addingnewmovementabilitiestocharactermovement)
        for the theory behind that.

-   If you are having trouble with a importing header files, regenerate
    your VS files

    -   A good troubleshooting step when running into odd issues is to
        rebuild the solution, as well.

-   I had trouble with the hosting a joining, in that it was giving me
    [this
    error](https://answers.unrealengine.com/questions/597582/failed-to-join-lan-session-cant-find-map-file.html)

    -   This was solved by deleting the on click events in the MainMenu
        widget blueprint, recompiling, close the editor, opening it
        again, and finally, adding the onclick events again.

        -   File -&gt; Refresh nodes can also be used to force a
            re-compile

    -   It would seem that this is a common fix to many strange
        blueprint errors.
