// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "TopDownBomberPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNBOMBER_API ATopDownBomberPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	ATopDownBomberPlayerState();

	// This player's number
	UPROPERTY(Replicated)
	int PlayerNumber;

	// this player's score
	UPROPERTY(ReplicatedUsing=OnRep_PlayerScore)
	int PlayerScore;

	// Update the UI when a player's score is changed by the server
	UFUNCTION()
	void OnRep_PlayerScore();
};
