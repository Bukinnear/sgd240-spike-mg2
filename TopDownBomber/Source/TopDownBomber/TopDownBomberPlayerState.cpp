// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownBomber.h"
#include "TopDownBomberPlayerState.h"
#include "UnrealNetwork.h"
#include "TopDownBomberPlayerController.h"

ATopDownBomberPlayerState::ATopDownBomberPlayerState()
{
	
}


void ATopDownBomberPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATopDownBomberPlayerState, PlayerNumber);
	DOREPLIFETIME(ATopDownBomberPlayerState, PlayerScore);
}

void ATopDownBomberPlayerState::OnRep_PlayerScore()
{
	Cast<ATopDownBomberPlayerController>(GetWorld()->GetFirstPlayerController())->RefreshScoreUI(this);
}