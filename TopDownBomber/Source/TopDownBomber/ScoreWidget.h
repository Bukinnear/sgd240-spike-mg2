// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "ScoreWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNBOMBER_API UScoreWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/**Called by the GameMode to set the player's number */
	UFUNCTION(BlueprintImplementableEvent)
	void SetPlayerNumber(int NewPlayerNumber);

	/**Sets the player's score to the given amount */
	UFUNCTION(BlueprintImplementableEvent)
	void SetPlayerScore(int NewPlayerScore);
	
protected:
	// Player's number
	UPROPERTY(BlueprintReadOnly, Category = "Player")
	int PlayerNumber;

	// Player's Score
	UPROPERTY(BlueprintReadOnly, Category = "Player")
	int PlayerScore;	
};