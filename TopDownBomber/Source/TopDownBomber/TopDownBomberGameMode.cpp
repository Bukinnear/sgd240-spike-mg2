// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TopDownBomber.h"
#include "TopDownBomberGameMode.h"
#include "TopDownBomberPlayerController.h"
#include "TopDownBomberCharacter.h"
#include "TopDownBomberGameState.h"
#include "TopDownBomberPlayerState.h"

ATopDownBomberGameMode::ATopDownBomberGameMode()
{
	InitDefaultClasses();
	bStartPlayersAsSpectators = true;
	NewPlayerNumber = 0;
	bCanBeginRound = false;
}

void ATopDownBomberGameMode::InitDefaultClasses()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDownBomberPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	GameStateClass = ATopDownBomberGameState::StaticClass();
	PlayerStateClass = ATopDownBomberPlayerState::StaticClass();
}

void ATopDownBomberGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	// Give the new player a unique number
	NewPlayerNumber++;
	ATopDownBomberPlayerController* NewPlayerController = Cast<ATopDownBomberPlayerController>(NewPlayer);
	ATopDownBomberPlayerState* NewPlayerState = Cast<ATopDownBomberPlayerState>(NewPlayerController->PlayerState);
	if (NewPlayerState)
	{
		NewPlayerState->PlayerNumber = NewPlayerNumber;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("Playernumber could not be assigned to %s"), *NewPlayer->GetName()); }
		
	if (bCanBeginRound && GetNumSpectators() + GetNumPlayers() >= 2)
	{
		FTimerHandle CountDownTimer;
		GetWorldTimerManager().SetTimer(CountDownTimer, this, &ATopDownBomberGameMode::BeginCountdown, 2.0f);
	}
}

void ATopDownBomberGameMode::BeginCountdown()
{
	ATopDownBomberGameState* OurGameState = Cast<ATopDownBomberGameState>(GameState);
	OurGameState->BeginRound();
	SpawnAllPawnsAndPossessControllers();
}

void ATopDownBomberGameMode::SpawnAllPawnsAndPossessControllers()
{
	if (HasAuthority())
	{		
		FConstPlayerControllerIterator ControllerIterator = GetWorld()->GetPlayerControllerIterator();

		if (ControllerIterator->IsValid())
		{
			while (ControllerIterator.operator bool())
			{
				APlayerController* CurrentController = ControllerIterator->Get();

				if (CurrentController->IsValidLowLevel())
				{
					auto PlayerStart = FindPlayerStart(CurrentController);
					APawn* newPawn = SpawnDefaultPawnFor(CurrentController, PlayerStart);
					CurrentController->Possess(newPawn);

					++ControllerIterator;
				}
			}
		}
	}
}

