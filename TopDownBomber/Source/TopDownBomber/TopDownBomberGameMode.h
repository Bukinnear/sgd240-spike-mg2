// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "TopDownBomberGameMode.generated.h"

UCLASS(minimalapi)
class ATopDownBomberGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATopDownBomberGameMode();
	void PostLogin(APlayerController* NewPlayer) override;

	/**This function spawns all pawns and then forces each PC to Possess the right one */
	UFUNCTION()
		virtual void SpawnAllPawnsAndPossessControllers();

protected:
	// Player's number
	UPROPERTY()
		int NewPlayerNumber;

	UPROPERTY(BlueprintReadWrite)
		bool bCanBeginRound;

	UFUNCTION()
		virtual void InitDefaultClasses();

	UFUNCTION()
		void BeginCountdown();
};