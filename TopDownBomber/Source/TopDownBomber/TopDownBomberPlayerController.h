// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "GameFramework/PlayerController.h"
#include "TopDownBomberPlayerController.generated.h"


UCLASS()
class ATopDownBomberPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	/**Constructor */
	ATopDownBomberPlayerController();
		
protected:

	UPROPERTY()
		/** True if the controlled character should navigate to the mouse cursor. */
		uint32 bMoveToMouseCursor : 1;

	UPROPERTY()
		// Our Score Widget class
		TSubclassOf<class UUserWidget> ScoreWidgetClass;
		
	UPROPERTY()
		// Our Loading screen widget class
		TSubclassOf<class UUserWidget> LoadingWidgetClass;
		
	UPROPERTY()
		// Our loading screen widget
		 class ULoadingWidget* LoadingScreenWidget;

	UPROPERTY()
		// Index of the countdown before the match starts
		int CountdownIndex;

	UPROPERTY()
		// Timer to act as the countdown
		FTimerHandle CountdownTimer;

	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	UFUNCTION()
		/** Navigate player to the current mouse cursor location. */
		void MoveToMouseCursor();

	UFUNCTION()
		/** Input handler for SetDestination action. */
		void OnSetDestinationPressed();

	UFUNCTION()
		/** Input handler for SetDestination action. */
		void OnSetDestinationReleased();

	UFUNCTION()
		/** Spwns a bomb at the Player's location. */
		void DropBomb();

	UPROPERTY()
		// Map array to hold our player states matched to their respective score display object
		TMap<const class ATopDownBomberPlayerState*, class UScoreWidget*> ScoreWidgetArray;
		
public:

	UFUNCTION()
		/**Called on BeginPlay to add all current players scores to the UI */
		void AddAllPlayerScores();

	UFUNCTION()
		/**Adds a player's score to the UI */
		void AddNewScoreUI(const class ATopDownBomberPlayerState* NewPlayerState);

	UFUNCTION()
		/**Refreshes the score UI assosiated with the given PlayerState */
		void RefreshScoreUI(ATopDownBomberPlayerState* PlayerStateToRefresh);

	UFUNCTION()
		/**Adds the given amount to this player's score - only affects UI and does not affect
		*actual game score. Required to update the server player's UI and combat latency in
		*client score updates. */
		void SetOwnScoreUI(int ScoreToSet);

	UFUNCTION(BlueprintCallable, Category = "PlayerController")
		/**Shows the loading screen at the beginning of the match */
		void ShowLoadingScreen();

	UFUNCTION()
		/**Countdown to the beginning of the round, Remove the loading screen, & enable player input */
		void RoundCountdown();

	UFUNCTION()
		/**Function for debugging */
		void Debug();
};