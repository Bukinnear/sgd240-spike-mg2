// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UnrealNetwork.h"
#include "GameFramework/GameStateBase.h"
#include "TopDownBomberGameState.generated.h"

/**
*
*/
UCLASS()
class TOPDOWNBOMBER_API ATopDownBomberGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	UFUNCTION(NetMulticast, Reliable, WithValidation)
		void BeginRound();
};
