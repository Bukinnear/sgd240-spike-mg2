// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownBomber.h"
#include "TopDownBomberGameState.h"
#include "TopDownBomberPlayerController.h"


void ATopDownBomberGameState::BeginRound_Implementation()
{	
	ATopDownBomberPlayerController* OurController = Cast<ATopDownBomberPlayerController>(UGameplayStatics::GetPlayerController(this, 0));
	OurController->RoundCountdown();
}

bool ATopDownBomberGameState::BeginRound_Validate()
{
	return true;
}